# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.14.7
#   kernelspec:
#     display_name: dga_data_model_x86
#     language: python
#     name: python3
# ---

# %%
import cx_Oracle
import pandas as pd

# %%
cx_Oracle.init_oracle_client(lib_dir=r"/Users/nghia/Downloads/instantclient_19_8/")

# %%
ip = '127.0.0.1'
port = 1521
SID = 'dgaworkzone'
dsn_tns = cx_Oracle.makedsn(ip, port, SID)

connection = cx_Oracle.connect('BASE', 'BaSe_Uat', dsn_tns)

# %%
query = """SELECT* 
           FROM DEMO_CUSTOMER"""
df_ora = pd.read_sql(query, con=connection)

# %%
df_ora.head()

# %%
